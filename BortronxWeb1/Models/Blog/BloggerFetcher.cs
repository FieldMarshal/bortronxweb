﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace BortronxWeb1.Models.Blog
{
    /// <summary>
    /// Retreive Data from www.Blogger.com
    /// </summary>
    /// <param name="host"></param>
    public class BloggerFetcher
    {
        public class Posts
        {
            public int totalItems { get; set; }
            public string selfLink { get; set; }
        }

        public struct BloggerBlog
        {
            public string name { get; set; }

            public Posts posts { get; set; }

        }

        /// <summary>
        /// Retrieve blogURL Data
        /// </summary>
        public async Task<List<BloggerPostData>> GetBlogPostData(string blogUrl, string _API_Key, int maxResults = 200)
        {

            string blogUri = "https://www.googleapis.com/blogger/v3/blogs/byurl?url=" + blogUrl + "&key=" + _API_Key;

            var httpClient = new HttpClient();

            using var releasesResponse = await JsonDocument.ParseAsync(await httpClient.GetStreamAsync(blogUri));

            string connectionStatus = releasesResponse.RootElement.GetProperty("name").GetString();

            BloggerBlog Blog = JsonSerializer.Deserialize<BloggerBlog>(releasesResponse.RootElement.GetRawText());

            //System.Console.WriteLine("Blog Post link: " + Blog.name);

            //Get Posts
            //System.Console.WriteLine("Posts Link: " + Blog.posts.selfLink);
            using var releasesResponse2 = await JsonDocument.ParseAsync(await httpClient
                .GetStreamAsync(Blog.posts.selfLink + "?fetchImages=true&maxResults=" + maxResults + "&key=" + _API_Key));

            List<BloggerPostData> blogPostsData = JsonSerializer.Deserialize<List<BloggerPostData>>(releasesResponse2
                .RootElement.GetProperty("items").GetRawText());

            //System.Console.WriteLine("Blog Data Retrieved: " + releasesResponse2
            //    .RootElement.GetProperty("items").GetRawText());
            //System.Console.WriteLine("Pages Retrieved: " + blogPostsData.Count);

            return blogPostsData;
        }
    }
}
