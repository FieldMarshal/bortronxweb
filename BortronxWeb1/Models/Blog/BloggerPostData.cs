﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BortronxWeb1.Models.Blog
{
    public class BloggerPostData
    {
        private List<string> _labels;
        private List<ImageData> _images;
        private string _content;
        private AuthorData _author;
        private DateTimeOffset? _published;
        private string _title;

        public class AuthorData
        {
            private string _displayName;

            public string displayName
            {
                get => _displayName ??= "";
                set => _displayName = value;
            }
        }

        public class ImageData
        {
            private const int _defaultSeed = 139;

            public string _seedCharacters = "";

            public void SetDefaultImageUri()
            {
                Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                _seedCharacters = rgx.Replace(_seedCharacters, "");

                url = "https://picsum.photos/seed/" + _seedCharacters + "/1280/720";
                System.Console.WriteLine(url);
            }

            public string url { get; set; }
        }

        public string title
        {
            get => _title ??= "No Title";
            set => _title = value;
        }

        public DateTimeOffset? published
        {
            get => _published ??= new DateTimeOffset(DateTime.Today);
            set => _published = value;
        }

        public AuthorData author
        {
            get => _author ??= new AuthorData();
            set => _author = value;
        }

        public string content
        {
            get => _content ??= "No Content";
            set => _content = value;
        }

        public List<string> labels
        {
            get => _labels ??= new List<string>(){"other"};
            set => _labels = value;
        }

        public List<ImageData> images
        {
            get => _images;
            set
            {
                _images = value;
            }
        }


        public ImageData GetImageOrDefault()
        {
            if (images != null)
            {
                System.Console.WriteLine("Images available: " + _images.Count);
                return _images[0];
            }
            else
            {
                ImageData randomImage = new ImageData();
                randomImage._seedCharacters = _title.Replace(" ", String.Empty);
                randomImage.SetDefaultImageUri();

                System.Console.WriteLine(randomImage.url);
                return randomImage;
            }


        }
    }
}
