﻿using System;
using System.Collections.Generic;

namespace BortronxWeb1.Models.Blog
{
    public class EzBlogPageData
    {
        public int PageID 
        {
            get
            {
                return _pageID;
            }
            set
            {
                _pageID = value;

                if ((_pageID - 1) >= 0)
                {
                    PreviousPostLink = BlogMainPageLink + "/Page/" + (_pageID - 1);
                }
                else
                {
                    PreviousPostLink = "";
                }

                NextPostLink = BlogMainPageLink + "/Page/" + (_pageID + 1);
            }
        }

        private int _pageID;

        public EzBlogPageData(int pageNum, string author, string title, string date, string commaSeparatedTags,
            string blogMainPage, string headImageUrl, string customHtml)
        {
            PageID = pageNum;
            Author = author;
            Title = title;
            Date = date;
            Tags = new List<string>(commaSeparatedTags.Split(", "));
            BlogMainPageLink = blogMainPage;
            HeadImageUrl = headImageUrl;
            CustomHtml = customHtml;
        }

        public EzBlogPageData(int pageNum, string author, string title, string date, List<string>  tags,
            string blogMainPage, string headImageUrl, string customHtml)
        {
            PageID = pageNum;
            Author = author;
            Title = title;
            Date = date;
            Tags = tags;
            BlogMainPageLink = blogMainPage;
            HeadImageUrl = headImageUrl;
            CustomHtml = customHtml;
        }



        public string Author { get; set; } = "Anonymous";

        public string Title { get; set; } = "Something Interesting!";

        public string Date { get; set; } = "Unknown Date";

        public List<string> Tags { get; set; }

        public string PreviousPostLink { get; set; }
        
        public string NextPostLink { get; set; }

        public string BlogMainPageLink { get; set; }

        public string HeadImageUrl { get; set; } 

        public string CustomHtml{ get; set; }

    }
}
