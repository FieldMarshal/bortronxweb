﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BortronxWeb1.Components.EzBlog
{
    public partial class EzBlogTagCollection
    {
        [Parameter]
        public string TagClass { get; set; } = "";

        [Parameter]
        public string BlogMainPageLink { get; set; }

        [Parameter]
        public List<string> Tags { get; set; }

        private List<Tuple<string, string>> postTags;

        public List<Tuple<string, string>> GetPostTags()
        {
            return postTags;
        }

        public Tuple<string, string> GetPostTags(int index)
        {
            if(index >= postTags.Count || index < 0)
            {
                return postTags[postTags.Count - 1];
            }

            return postTags[index];
        }

        public void SetPostTags(List<Tuple<string, string>> value)
        {
            postTags = value;
        }

        protected override void OnParametersSet()
        {
                List<Tuple<string, string>> tagsWithLink = new List<Tuple<string, string>>();

                foreach (string tag in Tags)
                {
                    string tagLink = BlogMainPageLink + "/Tag/" + tag;

                    // Re initialize the object whenever the parameter is smaller
                    // than its last size leaving the old object for the garbage
                    // collector to destroy.
                    if(GetPostTags() != null && Tags.Count < GetPostTags().Count)
                    {
                        SetPostTags(new List<Tuple<string, string>>());
                    }

                    tagsWithLink.Add(Tuple.Create(tag, tagLink));
                }

                SetPostTags(tagsWithLink);
        }

        public void SelectTag(string link)
        {

        }

    }
}
