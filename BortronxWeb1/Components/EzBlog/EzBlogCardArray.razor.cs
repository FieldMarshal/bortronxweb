﻿using BortronxWeb1.Models.Blog;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;


namespace BortronxWeb1.Components.EzBlog
{
    public partial class EzBlogCardArray
    {
        [Parameter]
        public EventCallback<int> OnClickCallback { get; set; }

        [Parameter]
        public List<EzBlogPageData> PageData { get; set; } = new List<EzBlogPageData>();

        [Parameter]
        public string PageFilter { get; set; }

        private bool isTagFound;

        private bool ContainsString(List<string> stringArray, string stringToCheck)
        {
            bool isFound = false;

            foreach (string x in stringArray)
            {
                if (x.Contains(stringToCheck))
                {
                    isFound = true;
                }
            }

            return isFound;
        }


    }

}
