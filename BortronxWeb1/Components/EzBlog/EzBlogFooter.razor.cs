﻿using Microsoft.AspNetCore.Components;

namespace BortronxWeb1.Components.EzBlog
{
    public partial class EzBlogFooter
    {
        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Parameter]
        public string PreviousPostLink { get; set; }

        [Parameter]
        public string BlogMainPageLink { get; set; }

        [Parameter]
        public string NextPostLink { get; set; }

        public bool isNextAvailable;
        public bool isPreviousAvailable;

        protected override void OnParametersSet()
        {
            isPreviousAvailable = (PreviousPostLink != "");
            isNextAvailable = (NextPostLink != "");
        }


        public void ReturnToBlogHome()
        {
            System.Console.WriteLine("Browse All Post Selected!");
        }

        public void NavigateToNextPost()
        {
            NavigationManager.NavigateTo(NextPostLink);
        }

        public void NavigateToPreviousPost()
        {
            NavigationManager.NavigateTo(PreviousPostLink);
        }
    }
}
