﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BortronxWeb1.Components.Utilities;
using Microsoft.AspNetCore.Components;
using StringAndCharModify;

namespace BortronxWeb1.Pages
{
    public partial class Utilities
    {
        [Parameter]
        public string SelectedUtility { get; set; }

        [Inject]
        NavigationManager uriHelper { get; set; }

        Dictionary<string, ComponentBase> UtilityComponents = new Dictionary<string, ComponentBase>()
        {
            {"ReverseList", new ReverseList() }
        };

        public string AddSpaces(string text)
        {
            return RegexFaster.AddSpacesToSentence(text);
        }

        public void NavigateToItem(string value)
        {
            uriHelper.NavigateTo("/Utilities/" + value);
        }
        public void NavigateBack()
        {
            uriHelper.NavigateTo("Utilities");
        }

    }
}
