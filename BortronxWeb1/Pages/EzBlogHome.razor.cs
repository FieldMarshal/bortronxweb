﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Xml.Serialization;
using BortronxWeb1.Models;
using BortronxWeb1.Models.Blog;
using BortronxWeb1.Components.EzBlog;
using Microsoft.AspNetCore.Components;
using System.Globalization;

namespace BortronxWeb1.Pages
{
    public partial class EzBlogHome
    {
        public int CurrentPageId { get; set; }

        [Parameter]
        public string BlogSection { get; set; }

        [Parameter]
        public string SectionArgument { get; set; }


        [Inject] NavigationManager uriHelper { get; set; }


        public string BlogHomePageLink;

        public List<EzBlogPageData> BlogPages = new List<EzBlogPageData>();

        public EzBlogPageData CurrentPage;

        public string currentAuthor = "Leonardo Guibert";

        

        private int _nextPageNumber = 1;

        private bool _isLoading = true;

        protected override async Task OnInitializedAsync()
        {
            BlogHomePageLink = uriHelper.ToAbsoluteUri("blog").AbsoluteUri;

            await InitializePagesManually();

            CurrentPage = BlogPages[0];

            BlogPages[BlogPages.Count - 1].NextPostLink = "";

            _isLoading = false;
        }

        protected override void OnParametersSet()
        {
            StateHasChanged();
        }

        public void SelectPage(int pageID)
        {
            CurrentPage = FindPageData(pageID);
            uriHelper.NavigateTo(BlogHomePageLink + "/Page/" + pageID);
            StateHasChanged();
        }

        private bool CheckPageExists(string pageData)
        {
            if (Int32.TryParse(pageData, out int num) && num < BlogPages.Count)
            {
                CurrentPageId = num;
                return true;
            }
                
            return false;
        }

        private EzBlogPageData FindPageData(int pageID)
        {
            return BlogPages[pageID];
            //for (var index = 0; index < BlogPages.Count; index++)
            //{
            //    EzBlogPageData pageData = BlogPages[index];

            //    if (pageData.PageID == pageID)
            //    {
            //        return pageData;
            //    }
            //}

            //if (pageID < BlogPages.Count)
            //{
            //    return BlogPages[pageID];
            //}

            //return null;
        }

        //Create Pages Manually
        public async Task InitializePagesManually()
        {
            BloggerFetcher fetcher = new BloggerFetcher();

            List<BloggerPostData> bloggerPostData = await fetcher.GetBlogPostData("http://leonardoguibert.blogspot.com/",
                "AIzaSyCwJYU9Zfd8tOshTciClx7L6-C-bpoa7GE");


            for (int i = 0; i < bloggerPostData.Count; i++)
            {
                BloggerPostData post = bloggerPostData[i];

                string date = post.published.Value.Date.ToString("MM/dd/yyyy");

                BlogPages.Add(new EzBlogPageData(_nextPageNumber, post.author.displayName, post.title, 
                    date, post.labels, BlogHomePageLink,
                    post.GetImageOrDefault().url, post.content));
               
                _nextPageNumber++;
            }

            BlogPages.Sort((ps2, ps1 ) => DateTime.Compare(DateTime.ParseExact(ps2.Date, "MM/dd/yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(ps1.Date, "MM/dd/yyyy", CultureInfo.InvariantCulture) ));;

            for (int i = 0; i < BlogPages.Count; i++)
            {
                EzBlogPageData item = (EzBlogPageData)BlogPages[i];
                item.PageID = i;
            }

        }

    }
}
