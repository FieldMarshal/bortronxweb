﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace ResourseFileEditor
{
    public class ResourceEditor
    {
        public void GenerateResourceFileBindings()
        {
            foreach (var item in collection)
            {
                
            }
        }

        string fileContent;

        async Task<Exception> FilesReadyForContent(IMatFileUploadEntry[] files)
        {
            try
            {
                var file = files.FirstOrDefault();
                if (file == null)
                {
                    return;
                }

                using (var stream = new MemoryStream())
                {
                    var sw = Stopwatch.StartNew();
                    await file.WriteToStreamAsync(stream);
                    sw.Stop();
                    if (stream.Length > 1024 * 1024)
                    {
                        fileContent = "";
                        fileContent += $"Name:\t{file.Name}\r\n";
                        fileContent += $"Type:\t{file.Type}\r\n";
                        fileContent += $"LastModified:\t{file.LastModified}\r\n";
                        fileContent += $"Size:\t{file.Size}\r\n";
                        fileContent += $"Time:\t{sw.Elapsed}\r\n";
                        fileContent += $"Speed:\t{(stream.Length / sw.Elapsed.TotalSeconds):N0}  b/s\r\n";
                    }
                    else
                    {
                        stream.Seek(0, SeekOrigin.Begin);
                        using (var reader = new StreamReader(stream))
                        {
                            fileContent = await reader.ReadToEndAsync();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return e;
            }

            return null;
        }
    }
}
