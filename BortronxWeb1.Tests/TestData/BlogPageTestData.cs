﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BortronxWeb1.Models.Blog;
using MatBlazor;

namespace BortronxWeb1.Tests.TestData
{
    class BlogPageTestData
    {
        public static List<string> TestTags = new List<string>()
        {
            "Programming",
            "Art",
            "C#",
            "Unity",
            "VR",
            "3D",
            "Articles",

        };

        public static List<EzBlogPageData> BlogPages = new List<EzBlogPageData>()
        {
             new EzBlogPageData(
                 2,
                 "testPerson",
                 "Impression VR (Image Generation)",
                 "Feb 10, 2018",
                 GetJoinedTagsWithComma(TestTags, new List<int> { 0, 1, 2, 3, 4, 5, }),
                 "http://localhost",
                 "https://drive.google.com/uc?export=view&id=1i1ITd3cvvIoJs0kz_vdqKoUYGaaXsYYT",
                 CustomHtmlData.ImpressionVR
                 ),

             new EzBlogPageData(
                 3,
                 "testPerson",
                 "WebbyGame",
                 "May 19, 2019",
                 GetJoinedTagsWithComma(TestTags, new List<int> { 6, 0, 1, 2, 4, 5 }),
                 "http://localhost",
                 "https://drive.google.com/uc?export=view&id=1i1ITd3cvvIoJs0kz_vdqKoUYGaaXsYYT",
                 CustomHtmlData.WebbyGame
                 ),

            new EzBlogPageData(
                4,
                "testPerson",
                "REDVR Reall-time Engineering Decision VR",
                "Dec 19, 2020",
                GetJoinedTagsWithComma(TestTags, new List<int> { 6, 0, 4, 5 }),
                "http://localhost",
                "https://drive.google.com/uc?export=view&id=1i1ITd3cvvIoJs0kz_vdqKoUYGaaXsYYT",
                CustomHtmlData.REDVR
                )
        };

        public static string GetJoinedTagsWithComma(List<string> tagList, List<int> selectedIndexes)
        {
            string joinedTagsWithComma = "";

            for (int i = 0; i < tagList.Count; i++)
            {
                bool isSelectedIndex = false;

                for (int j = 0; j < selectedIndexes.Count; j++)
                {
                    if (i == selectedIndexes[j])
                    {
                        isSelectedIndex = true;
                        selectedIndexes.RemoveAt(j);
                        break;
                    }
                }

                if (isSelectedIndex)
                {
                    joinedTagsWithComma += (tagList[i] + ", ");
                }
               
            }

            return joinedTagsWithComma;
        }

        public static string GetJoinedTagsWithComma(List<string> tagList)
        {
            string joinedTagsWithComma = "";

            for (int i = 0; i < tagList.Count; i++)
            {
                joinedTagsWithComma += (tagList[i] + ", ");
            }

            return joinedTagsWithComma;
        }
    }
}
