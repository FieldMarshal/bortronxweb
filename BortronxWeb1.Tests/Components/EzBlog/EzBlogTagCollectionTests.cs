﻿using AngleSharp.Common;
using BortronxWeb1.Models.Blog;
using BortronxWeb1.Models.Blog.Tests;
using BortronxWeb1.Tests.TestData;
using Bunit;
using MatBlazor;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;


namespace BortronxWeb1.Components.EzBlog.Tests
{
    public class EzBlogTagCollectionTests
    {
        [Test]
        public void EzBlogCardArrayTest_RendersCorrectly()
        {
            // Arrange
            using var ctx = new Bunit.TestContext();

            var linkedTags = new List<Tuple<string, string>>(){
                Tuple.Create(BlogPageTestData.TestTags[0], "test"),
                Tuple.Create(BlogPageTestData.TestTags[1], "test2")
            };

            var tags = new List<string>() 
            { 
                BlogPageTestData.TestTags[0],
                BlogPageTestData.TestTags[1]
            };

            string link = BlogPageTestData.BlogPages[0].BlogMainPageLink;

            // Act            
            var cut = ctx.RenderComponent<EzBlogTagCollection>(parameters => parameters
              .Add(p => p.BlogMainPageLink, link)
              .Add(p => p.Tags, tags)
            );

            // Assert
            var blogTags = cut.FindComponents<EzBlogTag>();

            Assert.AreEqual(2, blogTags.Count);
        }
    }
}