﻿using AngleSharp.Common;
using BortronxWeb1.Models.Blog;
using BortronxWeb1.Models.Blog.Tests;
using BortronxWeb1.Tests.TestData;
using Bunit;
using MatBlazor;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace BortronxWeb1.Components.EzBlog.Tests
{
    public class EzBlogCardArrayTest
    {
        [Test]
        public void EzBlogCardArrayTest_RendersCorrectly()
        {
            // Arrange
            using var ctx = new Bunit.TestContext();

            var BlogPages = BlogPageTestData.BlogPages;

            string sectionArgument = BlogPageTestData.TestTags.FirstOrDefault(s => s.Contains("Articles"));

            // <EzBlogCardArray PageData="@BlogPages" OnClickCallback="SelectPage" PageFilter="@SectionArgument"> </EzBlogCardArray>
            //var componentParameter = ComponentParameterFactory.Parameter("PageData", BlogPages);

            // Act            
            var cut = ctx.RenderComponent<EzBlogCardArray>(parameters => parameters
              .Add(p => p.PageData, BlogPages)
              .Add(p => p.PageFilter, sectionArgument)
            );

            // Assert
            var blogCards = cut.FindComponents<MatCard>();

            Assert.AreEqual(2, blogCards.Count);
        }

        [Test]
        public void EzBlogCardArrayTest_UpdatesCorrectly()
        {
            // Arrange
            using var ctx = new Bunit.TestContext();

            var BlogPages = BlogPageTestData.BlogPages;

            string sectionArgument1 = BlogPageTestData.TestTags.FirstOrDefault(s => s.Contains("Articles"));

            string sectionArgument2 = BlogPageTestData.TestTags.FirstOrDefault(s => s.Contains("Unity"));

            // <EzBlogCardArray PageData="@BlogPages" OnClickCallback="SelectPage" PageFilter="@SectionArgument"> </EzBlogCardArray>
            //var componentParameter = ComponentParameterFactory.Parameter("PageData", BlogPages);

            // Act            
            var cut = ctx.RenderComponent<EzBlogCardArray>(parameters => parameters
              .Add(p => p.PageData, BlogPages)
              .Add(p => p.PageFilter, sectionArgument1)
            );

            var cut2 = ctx.RenderComponent<EzBlogCardArray>(parameters => parameters
             .Add(p => p.PageData, BlogPages)
             .Add(p => p.PageFilter, sectionArgument2)
           );


            // Assert
            var blogCards1 = cut.FindComponents<MatCard>();
            var blogCards2 = cut2.FindComponents<MatCard>();

           
            Assert.AreEqual(2, blogCards1.Count);
            Assert.AreEqual(1, blogCards2.Count);
        }
    }
}
