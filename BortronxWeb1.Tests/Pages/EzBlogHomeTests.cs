﻿using BortronxWeb1.Models.Blog;
using NUnit.Framework;
using System.Collections.Generic;

namespace BortronxWeb1.Pages.Tests
{
    [TestFixture()]
    public class EzBlogHomeTests
    {
        [Test()]
        public void InitializePagesManually()
        {
            int _nextPageNumber = 1;
            string currentAuthor = "test";
            string BlogHomePageLink = "link";

            List<EzBlogPageData> BlogPages = new List<EzBlogPageData>();

            List<BloggerPostData> bloggerPostData = new List<BloggerPostData>()
            {

            };

            List<string> dates = new List<string>
            {
                "01/16/2020",
                "08/05/2019",
                "10/11/2018",
                "02/11/2018",
                "01/20/2017",
                "09/10/2017",
                "06/16/2017",
                "02/27/2016",
                "03/30/2015",
                "06/07/2018"
            };

            for (int i = 0; i<bloggerPostData.Count; i++)
            {
                BloggerPostData post = bloggerPostData[i];

                string date = post.published.Value.Date.ToString("d");

                if (i<dates.Count)
                {
                    date = dates[i];
                }

                BlogPages.Add(new EzBlogPageData(_nextPageNumber, post.author.displayName, post.title,
                    date, post.labels, BlogHomePageLink,
                    post.GetImageOrDefault().url, post.content));

                _nextPageNumber++;
            }

            BlogPages.Add(new EzBlogPageData(_nextPageNumber, currentAuthor, "Impression VR (Image Generation)", "Feb 10, 2018", "Programming, Art, C#, Unity, VR, 3D ", BlogHomePageLink, "https://drive.google.com/uc?export=view&id=1i1ITd3cvvIoJs0kz_vdqKoUYGaaXsYYT", CustomHtmlData.ImpressionVR));

            _nextPageNumber++;

            Assert.Fail("This failed");
        }
    }
}